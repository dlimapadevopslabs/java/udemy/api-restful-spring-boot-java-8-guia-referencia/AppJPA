package com.kazale.jpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kazale.jpa.entity.Empresa;

//Recurso generics. Digo a classe e a chave
public interface EmpresaRepository extends JpaRepository<Empresa, Long>{
	
	//definição de um novo método que pega o cnpj como parâmetro
	Empresa findByCnpj(String cnpj);
	
}