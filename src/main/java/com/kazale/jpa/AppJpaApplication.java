package com.kazale.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.kazale.jpa.entity.Empresa;
import com.kazale.jpa.repositories.EmpresaRepository;

import ch.qos.logback.classic.net.SyslogAppender;

@SpringBootApplication
public class AppJpaApplication {
	
	@Autowired
	private EmpresaRepository empresaRepository;

	public static void main(String[] args) {
		SpringApplication.run(AppJpaApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner comandLineRunner() {
		return args -> {
			//criando a empresa
			Empresa empresa = new Empresa();
			empresa.setRazaoSocial("Kazale IT");
			empresa.setCnpj("12312312312399");
			
			//persistindo no banco de dados
			this.empresaRepository.save(empresa);
			
			//recuperando as empresas do banco de dados
			List<Empresa> empresas = empresaRepository.findAll();
			//sintaxe do java 8
			empresas.forEach(System.out::println);
			
			//recuperando pela chave
			Empresa empresaDB = empresaRepository.findOne(1L);
			System.out.println("Empresa por ID: " + empresaDB);
			
			empresaDB.setRazaoSocial("Kazale IT Web");
			this.empresaRepository.save(empresaDB);
			
			//recuperando pelo cnpj
			Empresa empresaCnpj = empresaRepository.findByCnpj("12312312312399");
			System.out.println("Empresa por CNPJ: " + empresaCnpj);
			
			//pagando do banco
			this.empresaRepository.delete(1L);
			empresas = empresaRepository.findAll();
			System.out.println("Empresas: " + empresas.size());
		};
	}
}
